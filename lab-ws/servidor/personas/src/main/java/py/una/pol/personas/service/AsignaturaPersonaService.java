package py.una.pol.personas.service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.dao.AsignaturaPersonaDAO;
import py.una.pol.personas.model.AsignaturaPersona;

@Stateless
public class AsignaturaPersonaService {
	@Inject
    private Logger log;

    @Inject
    private AsignaturaPersonaDAO dao;
    
    public void asociar(AsignaturaPersona a) throws Exception{
    	log.info("Asociando Asignatura con Persona: " );//+ a.getNombre() );
        try {
        	dao.asociar(a);
        }catch(Exception e) {
        	log.severe("ERROR al asociar asignatura con persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura asociada con éxito: " );//+ a.getNombre());
    }
    public void desasociar(AsignaturaPersona a) throws Exception{
    	log.info("Desasociando Asignatura con Persona: " );//+ a.getNombre() );
        try {
        	dao.desasociar(a.getIdAsignaturaPersona(),a.getCedula());
        }catch(Exception e) {
        	log.severe("ERROR al desasociar asignatura con persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura desasociada con éxito: " );//+ a.getNombre());
    }
	public List<String> personaPorAsignatura(long idAsignatura) {
		log.info("Obteniendo todas las personas asociadas a esta asignatura: " );//+ a.getNombre() );
		List<String> lista= new ArrayList<String>();
        try {
        	lista=dao.listarPorAsignatura(idAsignatura);//odesasociar(a.getIdAsignaturaPersona(),a.getCedula());
        }catch(Exception e) {
        	log.severe("ERROR al obtener personas: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Personas obtenidas con éxito: " );//+ a.getNombre());
		return lista;
	}
	public List<String> asignaturaPorPersona(long cedula) {
		log.info("Oteniendo asignaturas asociadas a esta persona: "+cedula );//+ a.getNombre() );
		List<String> lista= new ArrayList<String>();
        try {
        	lista=dao.listarPorPersona(cedula);//odesasociar(a.getIdAsignaturaPersona(),a.getCedula());
        }catch(Exception e) {
        	log.severe("ERROR al obtener  las asignaturas: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignaturas obtenidas con éxito: " );//+ a.getNombre());
		return lista;
	}
	public boolean existeAsociacion(AsignaturaPersona a) {
		try {
        	return dao.existeAsociacion(a.getCedula(),a.getIdAsignaturaPersona());
        }catch(Exception e) {
        	log.severe("ERROR al obtener la asociación: " + e.getLocalizedMessage() );
        	throw e;
        }
		
	}

}
