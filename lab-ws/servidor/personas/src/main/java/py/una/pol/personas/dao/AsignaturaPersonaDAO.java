package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.AsignaturaPersona;

public class AsignaturaPersonaDAO {
	@Inject
    private Logger log;
	
	//1. CREAR
		public long asociar(AsignaturaPersona a) throws SQLException{
			String SQL = "INSERT INTO asignatura_persona(cedula,id_asignatura) "
	                + "VALUES(?,?)";
	 
	        long id = 0;
	        Connection conn = null;
	        
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
	            pstmt.setLong(1, a.getCedula());
	            pstmt.setLong(2, a.getIdAsignaturaPersona());
	            
	 
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getLong(1);
	                    }
	                } catch (SQLException ex) {
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
	        	
	        return id;		
		}
		
		//2. DESASOCIAR
		public long desasociar(long idAsignatura,long cedula) throws SQLException {

	        String SQL = "DELETE FROM asignatura_persona WHERE id_asignatura = ? "
	        		+ "and cedula= ?";
	 
	        long id = 0;
	        Connection conn = null;
	        
	        try 
	        {	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(SQL);
	            pstmt.setLong(1, idAsignatura);
	            pstmt.setLong(2, cedula);
	            int affectedRows = pstmt.executeUpdate();
	            // check the affected rows 
	            if (affectedRows > 0) {
	                // get the ID back
	                try (ResultSet rs = pstmt.getGeneratedKeys()) {
	                    if (rs.next()) {
	                        id = rs.getLong(1);
	                    }
	                } catch (SQLException ex) {
	                	log.severe("Error en la desasociación: " + ex.getMessage());
	                	throw ex;
	                }
	            }
	        } catch (SQLException ex) {
	        	log.severe("Error en la desasociación: " + ex.getMessage());
	        	throw ex;
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        		throw ef;
	        	}
	        }
	        return id;
	    }
		
		//3. LISTAR POR PERSONA
		public List<String> listarPorPersona(long cedula) {
			String query = "select a.nombre from asignatura_persona ap, asignatura a "
					+ " where ap.cedula = ?"
					+ " and a.id_asignatura = ap.id_asignatura";
			List<String> lista = new ArrayList<String>();
			System.out.println("consulta: "+ query);
			Connection conn = null; 
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(query);
	            pstmt.setLong(1, cedula);
	        	ResultSet rs = pstmt.executeQuery();
	        	while(rs.next()) {
	        		String a =rs.getString(1);
	        		//a.setIdAsignatura(rs.getLong(1));
	        		//a.setNombre(rs.getString(2));
	        		lista.add(a);
	        	}
	        	
	        } catch (SQLException ex) {
	            log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return lista;

		}

		//4 LISTAR POR ASIGNATURA
		public List<String> listarPorAsignatura(long idAsignatura) {
			String query = "select p.nombre from asignatura_persona ap, persona p"
					+ " where ap.id_asignatura= ?"
					+ " and p.cedula= ap.cedula";
			List<String> lista = new ArrayList<String>();
			Connection conn = null; 
			log.info("consulta: "+query);
	        try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(query);
	            pstmt.setLong(1, idAsignatura);
	        	ResultSet rs =  pstmt.executeQuery();
	        	while(rs.next()) {
	        		String a =rs.getString(1);
	        		//a.setIdAsignatura(rs.getLong(1));
	        		//a.setNombre(rs.getString(2));
	        		lista.add(a);
	        	}
	        	
	        } catch (SQLException ex) {
	            log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return lista;
		}

		public boolean existeAsociacion(Long cedula, Long idAsignaturaPersona) {
			String sql="SELECT cedula FROM asignatura_persona where cedula= ? and id_asignatura=? ";
			Connection conn = null; 
			boolean existe=false;
			try 
	        {
	        	conn = Bd.connect();
	        	PreparedStatement pstmt = conn.prepareStatement(sql);
	            pstmt.setLong(1, cedula);
	            pstmt.setLong(2, idAsignaturaPersona);
	        	ResultSet rs =  pstmt.executeQuery();
	        	while(rs.next()) {
	        		existe= true;
	        	}
	        	
	        } catch (SQLException ex) {
	            log.severe("Error en la seleccion: " + ex.getMessage());
	        }
	        finally  {
	        	try{
	        		conn.close();
	        	}catch(Exception ef){
	        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
	        	}
	        }
			return existe;
		}
}
