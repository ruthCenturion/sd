package py.una.pol.personas.service;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {
	@Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre() );
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre());
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorIdAsignatura(long idAsignatura) {
    	return dao.seleccionarPorIdAsignatura(idAsignatura);
    }
    
    public long borrar(long idAsignatura) throws Exception {
    	return dao.borrar(idAsignatura);
    }
    public void actualizar(Asignatura a) throws Exception {
    	log.info("Modificando Asignatura: " + a.getNombre() );
        try {
        	dao.actualizar(a);
        }catch(Exception e) {
        	log.severe("ERROR al modificar asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Asignatura modificada con éxito: " + a.getNombre());
    }

	public boolean existeAsignatura(Asignatura a) {
		boolean existe=false;
		try {
        	existe=dao.existeAsignatura(a);
        }catch(Exception e) {
        	log.severe("ERROR al obtener asignatura: " + e.getLocalizedMessage() );
        	throw e;
        }
		return existe;
	}

}
