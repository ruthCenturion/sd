package py.una.pol.personas.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;


@Stateless
public class AsignaturaDAO {
	@Inject
    private Logger log;
	
	//1. CREAR
	public long insertar(Asignatura a) throws SQLException{
		String SQL = "INSERT INTO asignatura(id_asignatura, nombre) "
                + "VALUES(?,?)";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, a.getIdAsignatura());
            pstmt.setString(2, a.getNombre());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        	
        return id;		
	}
	//2. ACTUALIZAR
	public long actualizar(Asignatura a) throws SQLException {

        String SQL = "UPDATE asignatura SET nombre = ?  WHERE id_asignatura = ? ";
 
        long id = 0;
        Connection conn = null;
        Asignatura aux=seleccionarPorIdAsignatura(a.getIdAsignatura());
        if(aux== null){
        	log.severe("Asignatura: "+a.getIdAsignatura()+ " no encontrada. No se puede modificar");
        	throw new WebApplicationException(Response.Status.NOT_FOUND+ ". Asignatura: "+a.getIdAsignatura()+ " no encontrada. No se puede modificar");
        }
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getNombre());
            pstmt.setLong(2, a.getIdAsignatura());
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexión a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }
	
	//3. BORRAR
	public long borrar(long idAsignatura) throws SQLException {

        String SQL = "DELETE FROM asignatura WHERE id_asignatura = ? ";
 
        long id = 0;
        Connection conn = null;
        
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, idAsignatura);
 
            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexión a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
	//4. SELECCIONAR
	public List<Asignatura> seleccionar() {
		String query = "SELECT id_asignatura, nombre FROM asignatura ";
		List<Asignatura> lista = new ArrayList<Asignatura>();
		
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Asignatura a = new Asignatura(rs.getLong(1),rs.getString(2));
        		//a.setIdAsignatura(rs.getLong(1));
        		//a.setNombre(rs.getString(2));
        		lista.add(a);
        	}
        	
        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;

	}
	
	public Asignatura seleccionarPorIdAsignatura(long idAsignatura) {
		String SQL = "SELECT id_asignatura, nombre FROM asignatura WHERE id_asignatura = ? ";
		Asignatura a = null;
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(SQL);
        	pstmt.setLong(1, idAsignatura);
        	
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		a = new Asignatura(rs.getLong(1),rs.getString(2));
        		
        	}
        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return a;

	}
	public boolean existeAsignatura(Asignatura a) {
		String sql="SELECT id_asignatura, nombre FROM asignatura where id_asignatura = ? ";
		boolean existe=false;
		Connection conn = null; 
        try 
        {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(sql);
        	pstmt.setLong(1, a.getIdAsignatura());        	
        	ResultSet rs = pstmt.executeQuery();
        	while(rs.next()) {
        		existe= true;        		
        	}        	
        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try{
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return existe;
	}

	

}
