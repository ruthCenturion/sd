package py.una.pol.personas.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class AsignaturaPersona  implements Serializable {

	Long cedula;
	Long idAsignaturaPersona;
	
	public AsignaturaPersona(){
		//asignaturas = new ArrayList<String>();
	}

	public AsignaturaPersona(Long vCedula, Long id){
		this.cedula = vCedula;
		this.idAsignaturaPersona = id;
	}
	
	public Long getCedula() {
		return cedula;
	}

	public void setCedula(Long cedula) {
		this.cedula = cedula;
	}

	public Long getIdAsignaturaPersona() {
		return idAsignaturaPersona;
	}

	public void setIdAsignaturaPersona(Long idAsignaturaPersona) {
		this.idAsignaturaPersona = idAsignaturaPersona;
	}
	
}
