
/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.AsignaturaPersona;
import py.una.pol.personas.service.AsignaturaPersonaService;
import py.una.pol.personas.service.AsignaturaService;
import py.una.pol.personas.service.PersonaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de asignatura_persona
 */
@Path("/asignatura-persona")
@RequestScoped
public class AsignaturaPersonaRESTService {

    @Inject
    private Logger log;
    @Inject
    PersonaService personaService;
    @Inject
    AsignaturaService asignaturaService;
    @Inject
    AsignaturaPersonaService asignaturaPersonaService;

       /**

     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,

     * or with a map of fields, and related errors.

     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response asociar(AsignaturaPersona a) {

        Response.ResponseBuilder builder = null;

        try {
        	//verificar que exista PERSONA y ASIGNATURA
        	if(personaService.seleccionarPorCedula(a.getCedula())== null 
     	        	&& asignaturaService.seleccionarPorIdAsignatura(a.getIdAsignaturaPersona()) == null) {
	        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe persona ni asignatura para asociar.");
        	}else
        	//verificar que exista PERSONA
        	if(personaService.seleccionarPorCedula(a.getCedula())== null) {
        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe persona para asociar.");
 		    }else //verificar que exista ASIGNATURA
 	        	if(asignaturaService.seleccionarPorIdAsignatura(a.getIdAsignaturaPersona()) == null) {
 	        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe asignatura para asociar.");
 	        	}else if(asignaturaPersonaService.existeAsociacion(a)) {
 	 		    	builder= Response.status(Response.Status.FOUND).entity("Ya existe asociacion entre esta persona y asignatura.");
 	 		    }else {
 	 		    	
            asignaturaPersonaService.asociar(a);
            builder = Response.status(201).entity("Se ha asociado correctamente la asignatura");}
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response desasociar(AsignaturaPersona a) {
        Response.ResponseBuilder builder = null;
        try {
        	//verificar que exista PERSONA y ASIGNATURA
        	if(personaService.seleccionarPorCedula(a.getCedula())== null 
     	        	&& asignaturaService.seleccionarPorIdAsignatura(a.getIdAsignaturaPersona()) == null) {
	        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe persona ni asignatura para desasociar.");
        	}else
        	//verificar que exista PERSONA
        	if(personaService.seleccionarPorCedula(a.getCedula())== null) {
        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe persona para desasociar.");
 		    }else //verificar que exista ASIGNATURA
 	        	if(asignaturaService.seleccionarPorIdAsignatura(a.getIdAsignaturaPersona()) == null) {
 	        		builder= Response.status(Response.Status.NOT_FOUND).entity("No existe asignatura para adesasociar.");
 	 		    }else if(!asignaturaPersonaService.existeAsociacion(a)) {
 	 		    	builder= Response.status(Response.Status.NOT_FOUND).entity("No existe asociacion entre esta persona y asignatura para adesasociar.");
 	 		    }else {
 	 		    	
 	 		    	asignaturaPersonaService.desasociar(a);
			        builder = Response.status(201).entity("Se ha desasociado correctamente la asignatura");
 		    } 
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }
    
   @GET
   @Path("/{idAsignatura:[0-9][0-9]*}")
   @Produces(MediaType.APPLICATION_JSON)
   public List<String> personaPorAsignatura(@PathParam("idAsignatura") long idAsignatura) {
	   List<String> a = asignaturaPersonaService.personaPorAsignatura(idAsignatura);
       if (a == null) {
       	log.info("obtenerPorId " + idAsignatura + " no encontrado.");
           throw new WebApplicationException(Response.Status.NOT_FOUND);
       }
       log.info("obtenerPorId " + idAsignatura + " encontrada: ");// + a.getNombre());
       return a;
   }
   @GET
   @Path("/cedula")
   @Produces(MediaType.APPLICATION_JSON)
   public List<String> asignaturaPorPersona(@QueryParam("cedula") long cedula) {
	   List<String> a = asignaturaPersonaService.asignaturaPorPersona(cedula);
       if (a == null) {
       	log.info("asignaturaPorPersona " + cedula + " no encontrado.");
           throw new WebApplicationException(Response.Status.NOT_FOUND);
       }
       log.info("asignaturaPorPersona " + cedula + " encontrada: " );//+ a.getNombre());
       return a;
   }
}
