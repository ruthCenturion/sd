package py.una.pol.personas.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura implements Serializable {
	Long idAsignatura;
	String nombre;
	public Asignatura(){
	}
	public Asignatura(Long aIdAsignatura, String aNombre){
		this.idAsignatura = aIdAsignatura;
		this.nombre = aNombre;
	}
	
	public Long getIdAsignatura() {
		return idAsignatura;
	}
	public void setIdAsignatura(Long idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}
